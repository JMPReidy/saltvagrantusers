
johnr:         # ID Declaration
  user.present:          # state declaration
    - gid: 100
    - home: /home/johnr
    - shell: /bin/bash
    - require:
      - group: users
  ssh_auth.present:
    - user: johnr
    - source: salt://ssh_keys/johnr.id_rsa.pub
    - config: /%h/.ssh/authorized_keys

fredn:         # ID Declaration
  user.absent:          # state declaration
    - purge: true

users:
  group.present:
    - gid: 100
