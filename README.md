# Task 2 Requirements #

Note, for Task 1, see Task 1.docx file in this repo.

Automate the process of granting / revoking SSH access to a group of servers instances to a new
developer.

* Your submission should include the use of Vagrant with a workable demo.
* Your submission may use Saltstack (optional)
* Your submission may use Git (optional)
* Your submission may include a README.md (optional)

# Approach. #

1. Using vagrant build a centos VM as a salt master.
2. Identify salt master IP address and key.
3. Install one or more target servers as salt minions.
4. Identify required ssh public/private key pair.
5. Implement a salt state file, with user.present, ssh_auth.present state declarations.
6. Apply salt state.
7. Implement salt state file with user.absent declaration.
8. Apply this salt state 

# Step 1 - Salt Master. #
For this exerise a centos7 vagrant box running within VirtualBox, will be used as the master as it was quickest to develop and implement this on a Windows based PC.

 Alternatively a OSX or Linux system could be used and the salt master implemented directly on it.

Download local image of box:

    vagrant box add bento/centos-7.2 --provider virtualbox
Init 

    vagrant init bento/centos-7.2
Edit vagrant file to support private network:

    $ sed -e 's/# config.vm.network "private_network".*/config.vm.network "private_network", ip: "192.168.56.10"/' -i Vagrantfile

Start box:

    vagrant up
set hostname:

    hostname centos7Master
also set in /etc/hostname

#  Install Salt Master.
     yum update
accept the prompts.

Install salt-master - from repository, yum install:

    sudo yum install https://repo.saltstack.com/yum/redhat/salt-repo-latest-1.el7.noarch.rpm
    sudo yum  clean expire-cache
    sudo yum install salt-master

start master.

    sudo salt-master


#  Target System - centos7Slave1

On first slave:
(after building the same vagrant box and configuring private network).
    sudo yum install https://repo.saltstack.com/yum/redhat/salt-repo-latest-1.el7.noarch.rpm
    sudo yum  clean expire-cache
    sudo yum install salt-minion

##  Notes.
* Could provision salt minion in Vagrant file
* Could pre-generate key on master and seed that on the minion.

Set master address on minion:
    sudo sed -e 's/^#master: salt/master: 192.168.56.100/' -i minion

Set master public on minion:
first on master get the public key:

    [vagrant@centos7Master ~]$ sudo salt-key -F master
    Local Keys:
    master.pem:  7d:31:6b:1c:08:60:XX:XX:XX:XX:XX:da:cd:1b:32:66
    master.pub:  b9:89:38:f2:80:73:9d:94:61:2f:73:b6:15:2d:6c:64
set on minion in /etc/salt/minion file:

    sudo sed -e "s/^#master_finger: ''/master_finger: d7:30:64:9b:82:c8:5e:f1:0c:4c:d1:81:3d:92:ba:0c'/" -i minion

#  Start Minon and Accept the Key.
If not pre-seeded, accept the minion key:

    [root@centos7Master salt]# salt-key -L
    Accepted Keys:
    Denied Keys:
    Unaccepted Keys:
    centos7Slave1.localdomain
    Rejected Keys:
    [root@centos7Master salt]# salt-key -A
    The following keys are going to be accepted:
    Unaccepted Keys:
    centos7Slave1.localdomain
    Proceed? [n/Y] y
    Key for minion centos7Slave1.localdomain accepted.
    
test the connection:

    [root@centos7Master salt]# salt '*' test.ping
    centos7Slave1.localdomain:
    True
    [root@centos7Master salt]# salt '*' cmd.run 'hostname'
    centos7Slave1.localdomain:
    centos7Slave1.localdomain

# Configure User and SSH Key. #

Use salt-state to set the user and key as required.

Enable the share location - uncommentin /etc/salt/master:

    file_roots:
      base:
    - /srv/salt
    
Add files:

/srv/salt/top.sls

    base:
      '*':  # target minions
    - devusers
    
/srv/sale/devusers.sls


    johnr: # ID Declaration
      user.present:  # state declaration
    - gid: 100
    - home: /home/johnr
    - shell: /bin/bash
    - require:
      - group: users
      ssh_auth.present:
    - user: johnr
    - source: salt://ssh_keys/johnr.id_rsa.pub
    - config: /%h/.ssh/authorized_keys
    
    users:
      group.present:
    - gid: 100
    
Copy required ssh key to /srv/salt/keys, assume key is on local system:

    cp ~johnr/.ssh/id_rsa.pub /srv/salt/ssh_keys/johnr.id_rsa.pub



Apply the state:

    [root@centos7Master ssh_keys]# salt '*' state.apply
    centos7Slave1.localdomain:
    ----------
      ID: users
    Function: group.present
      Result: True
     Comment: Group users is present and up to date
     Started: 12:07:54.670029
    Duration: 1.597 ms
     Changes:
    ----------
      ID: johnr
    Function: user.present
      Result: True
     Comment: New user johnr created
     Started: 12:07:54.671832
    Duration: 54.179 ms
     Changes:
      ----------
      fullname:
      gid:
      100
      groups:
      - users
      home:
      /home/johnr
      homephone:
      name:
      johnr
      passwd:
      x
      roomnumber:
      shell:
      /bin/bash
      uid:
      1001
      workphone:
    ----------
      ID: johnr
    Function: ssh_auth.present
      Result: True
     Comment: The authorized host key johnr for user johnr was added
     Started: 12:07:54.726617
    Duration: 26.016 ms
     Changes:
      ----------
      johnr:
      New
    
    Summary for centos7Slave1.localdomain
    ------------
    Succeeded: 3 (changed=2)
    Failed:0
    ------------
    Total states run: 3
    Total run time:  81.792 ms
    

Test the user and key:

    [johnr@centos7Master ~]$ ssh 192.168.56.101
    The authenticity of host '192.168.56.101 (192.168.56.101)' can't be established.
    ECDSA key fingerprint is 93:63:07:33:57:9d:2f:7c:b8:31:ee:43:74:f0:37:d4.
    Are you sure you want to continue connecting (yes/no)? yes
    Warning: Permanently added '192.168.56.101' (ECDSA) to the list of known hosts.

Removing the user:

edit dev users - remove user **fredn**

    fredn: # ID Declaration
      user.absent:  # state declaration
    - purge: true
    

Output:

    ----------
          ID: fredn
    Function: user.absent
      Result: True
     Comment: Removed user fredn
     Started: 12:14:50.883419
    Duration: 42.93 ms
     Changes:
              ----------
              fredn:
                  removed

    Summary for centos7Slave1.localdomain
    ------------
    Succeeded: 4 (changed=1)
    Failed:    0
    ------------
    Total states run:     4
    Total run time:  88.103 ms
   